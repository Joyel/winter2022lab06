import java.util.Random;

public class Die
{
	private int pips;
	private Random rngRoll;
	
	public Die()
	{
		this.pips = 1;
		this.rngRoll = new Random();
	}
	
	public int getPips()
	{
		return this.pips;
	}
	
	public void roll()
	{
		this.pips = this.rngRoll.nextInt(6) + 1;
	}
	
	public String toString()
	{
		return "" + this.pips;
	}
}