public class Board
{
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board()
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
		
		/* Technically, a value of 1 can never be gotten from a roll since the lowest dice 
		   values (1 and 1) add to 2. So it would be boolean[11], since it's from 2 to 12.
		   But the importance is the concept, so I'll leave it as it is. 
		   
		   And actually, because we're working with arrays, the index starts at 0, which
		   potentially adds a layer of confusion to the closedTiles display. Ex if the dice
		   rolls 4, index 4 is actually position 5. So of course, a dice roll of 12 =
		   array index out of bounds, as the worst consequence. I accounted for this in the
		   code with appropriate i-1 and i+1 offsets. */
	}
	
	public String toString()
	{
		String tileStatus = "";
		
		for (int i = 0; i < this.closedTiles.length; i++)
		{
			if (this.closedTiles[i])
			{
				tileStatus += "X";
			}
			
			else
			{
				tileStatus += (i + 1);
			}
			
			
			//This if statement puts a space between each of the tile statuses
			if (i < this.closedTiles.length - 1)
			{
				tileStatus+= " ";
			}
		}
		
		return tileStatus;
	}
	
	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();
		
		System.out.println(this.die1);
		System.out.println(this.die2);
		
		int diceTotal = this.die1.getPips() + this.die2.getPips();
		
		System.out.println("Your dice rolled " + diceTotal + ". Checking respective tile number... ");
		
		if (this.closedTiles[diceTotal - 1])
		{
			System.out.println("This tile is already closed.");
			System.out.println("");
			return true;
		}
		
		else
		{
			this.closedTiles[diceTotal - 1] = true;
			System.out.println("Closing tile: " + diceTotal);
			System.out.println("");
			return false;
		}
		
	}
	
}